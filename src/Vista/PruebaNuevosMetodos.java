/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Interface.IMatriz;
import Interface.IMatriz2;
import Negocio.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class PruebaNuevosMetodos {
 
    public static void main(String[] args) {
        
        try {
            System.out.println("Por favor digite datos de la matriz:");
            Scanner lector = new Scanner(System.in);
            System.out.println("Por favor digite cantidad de filas:");
            int filas=lector.nextInt();
            System.out.println("Por favor digite cantidad de columnas:");
            int cols=lector.nextInt();
            System.out.println("Por favor digite limiInicial de elementos aleatorios:");
            int ini=lector.nextInt();
            System.out.println("Por favor digite limifinal de elementos aleatorios:");
            int fin=lector.nextInt();
            
            MatrizEntero_Aleatoria myMatriz=new MatrizEntero_Aleatoria(filas, cols);
            myMatriz.crearMatriz(ini, fin);
            System.out.println("Mi matriz:"+myMatriz.toString());
            
            
            //PRUEBAS DE LA INTERFAZ 2
            //No funciona la clase matriz entero, imprime 0
            //Clase MatrizEntero_Aleatoria
            System.out.println("Pruebas de la Interfaz 2: ");            
            System.out.println("Prueba del metodo getMas_Se_Repite() en la clase MatrizEntero_Aleatoria: ");  
            
            System.out.println("getMas_Se_Repite de la forma1:"+myMatriz.getMas_Se_Repite());
          
            //Forma 2:POLIMORFISMO
            IMatriz2 i_matriz2m1=myMatriz;
            System.out.println("getMas_Se_Repite de la POLIMORFISMO:"+i_matriz2m1.getMas_Se_Repite());
            
            //Forma 3:CASTING DE LA INTERFACE     
            int msr=((IMatriz2)myMatriz).getMas_Se_Repite();
            System.out.println("getMas_Se_Repite de la CASTING:"+msr);
            
            
            
            //Clase Par_Numero
            System.out.println("Prueba del metodo getMas_Se_Repite() en la clase Par_Numero: ");
            
            //Forma 2:POLIMORFISMO
            Par_Numero par2=new Par_Numero(5,5);
            IMatriz2 i_matriz2m2=par2;
            System.out.println("getMas_Se_Repite usando POLIMORFISMO:"+i_matriz2m2.getMas_Se_Repite());
            
            //Clase Vector Numero
            System.out.println("Prueba del metodo getMas_Se_Repite() en la clase VectorNumero ");
            
            //Forma2: POLIMORFISMO
            VectorNumero vector=new VectorNumero(4,ini,fin);
            IMatriz2 i3=vector;
            System.out.println("getMas_Se_Repite usando POLIMORFISMO:"+i3.getMas_Se_Repite());
            
        } catch (Exception ex) {
            System.err.println("Error:"+ex.getMessage());
        }
    }
}
