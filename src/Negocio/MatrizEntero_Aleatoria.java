/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Interface.IMatriz;
import Interface.IMatriz2;

/**
 *
 * @author madar
 */
public class MatrizEntero_Aleatoria  implements IMatriz, IMatriz2{
    
    private int matriz[][];
    private int limFinal;
    private int limInicial;

    /**
     * Constructor vacío
     */
    public MatrizEntero_Aleatoria() {
        //NO es necesario, sólo por definición:
        this.matriz=null;
    }
    
    
    /**
     *  Crea una matriz cuadrada o rectangular
     * @param n cantidad de filas
     * @param m  cantidad de columnas
     */
    public MatrizEntero_Aleatoria(int n, int m) throws Exception
    {
    if(n<=0 || m<=0)
        throw new Exception("No se puede crear matrices con tamaños negativos o cero");
    this.matriz=new int[n][m];
    }       
    
    /**
     * Método que crear una matriz aleatorio
     * @param limInicial valor inicial de generación del aleatorio
     * @param limFinal valor final de generación del aleatorio
     */
    public void crearMatriz(int limInicial, int limFinal) throws Exception
    {
    if (limInicial>=limFinal)
        throw new Exception("No se puede llenar la matriz, sus límites están fuera del intervalo");
    
    for(int i=0;i<this.matriz.length;i++)
    {
        //matriz[..][...]
        for(int j=0;j<this.matriz[i].length;j++)
        {
        this.matriz[i][j]=(int) Math.floor(Math.random()*(limInicial-limFinal+1)+limFinal);  
        }
    }
    }
    
    
public String toString()
{

    if(this.matriz==null)
        return "Matriz vacía";
    String msg="";
    for(int fila_vector[]:this.matriz)
    {
        for(int dato_columna:fila_vector)
            msg+=dato_columna+"\t";
     msg+="\n";
    }
return msg;
    
}


    @Override
    public int getSumaTotal() {
        if(this.matriz==null)
        return 0;
        
     int total=0;
     for(int fila_vector[]:this.matriz)
    {
        for(int dato_columna:fila_vector)
            total+=dato_columna;
    }
    
     return total;
    }
    
    @Override
    public int getMas_Se_Repite() {
        if(this.matriz==null)
        return 0; 
        int numMasRep=0;
        int contResult=0;
        
       
       
        for(int cont=this.limInicial; cont<=this.limFinal; cont++){
            int cont2=0;
            
            for (int[] matrizb: this.matriz) {
                for (int j = 0; j < matrizb.length; j++) {
                    int aux = matrizb[j];
                    if(aux==cont)cont2++;
                }
            }
            if(cont2>contResult){
                numMasRep=cont;
                contResult=cont2;
            }
        }
        return numMasRep;
    }
    
}
