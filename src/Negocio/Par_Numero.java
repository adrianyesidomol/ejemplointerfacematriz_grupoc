/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Interface.IMatriz;
import Interface.IMatriz2;

/**
 *
 * @author madar
 */
public class Par_Numero implements IMatriz,IMatriz2{
    
    private int x, y;

    public Par_Numero() {
    }

    public Par_Numero(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Par_Numero{" + "x=" + x + ", y=" + y + '}';
    }

    @Override
    public int getSumaTotal() {
        return this.x+this.y;
    }
    
    @Override
    public int getMas_Se_Repite() {
        int numRepetido = 0;
        
        if (this.x == this.y){
            
            numRepetido = this.x;
            return numRepetido;
            
        }else{   
            
            return 0;
        }
    }     
}
