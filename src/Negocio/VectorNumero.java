/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Interface.IMatriz;
import Interface.IMatriz2;

/**
 *
 * @author madar
 */
public class VectorNumero implements IMatriz,IMatriz2{
    
    int numeros[];
    private int limFinal;
    private int limInicial;

    public VectorNumero() {
    }
    
    
    public VectorNumero(int n, int limInicial, int limFinal) throws Exception {
        if(n<=0)
            throw new Exception("No se puede crear el vector:");
        this.numeros=new int[n];
        
        this.crearNumeros(n);
        this.limInicial=limInicial;
        this.limFinal=limFinal;
        
    }
    
    private void crearNumeros(int n)
    {
        
     for(int i=0;i<n;i++)   
     {
         this.numeros[i]=i;
     }
    
    }

    public int[] getNumeros() {
        return numeros;
    }

    public void setNumeros(int[] numeros) {
        this.numeros = numeros;
    }

    @Override
    public String toString() {
        
        if(this.numeros==null)
            return ("Vector vacío");
        String msg="";
        for(int dato:this.numeros)
            msg+=dato+"\t";
        
        return msg;
        
    }

    @Override
    public int getSumaTotal() {
        int t=0;
        for(int dato:this.numeros)
           t+=dato;
        
        return t;
    }
    @Override
    public int getMas_Se_Repite() {
        int num=0;
        int contNum=0;
        for (int i = this.limInicial; i <= this.limFinal; i++) {  
            
            int cont=0;
            
            for (int j = 0; j < this.numeros.length; j++) {
                
                if(numeros[j]==i) cont++;     
                
            }
            if(cont>contNum){
                
                contNum=cont;
                num=i;
            
            }
        }
        return num;
    }   
}
